package Animals;

import Map.Cell;

import java.util.Random;

/**
 * Represents an Elephant in the simulation, which is a type of Animal.
 */
public class Elephant extends Animal {

    /**
     * Random number generator for determining initial move speed and power.
     */
    Random rand = new Random();

    /**
     * Name of the elephant.
     */
    String name = "Elephant";

    /**
     * Creates a new Elephant at the specified position.
     *
     * @param positionX The X-coordinate of the initial position.
     * @param positionY The Y-coordinate of the initial position.
     * @param map       Reference to the map cells.
     */
    public Elephant(int positionX, int positionY, Cell[][] map) {
        super(150); // Default health for elephant
        super.moveSpeed = rand.nextInt(1) + 1; // Random initial move speed
        super.name = name;
        super.power = rand.nextInt(100) + 100; // Random initial power
        super.x = positionX;
        super.y = positionY;
        super.health = 150; // Default health for elephant
        super.name = "Elephant";
        super.map = map;
    }

    @Override
    public void run() {
        if(isAlive){
        attackIfClose(); // Check for nearby animals to attack
        walk();
        }
    }
}
