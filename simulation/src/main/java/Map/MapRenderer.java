package Map;

import Animals.Animal;
import Controllers.MapController;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
/**
 * This class is responsible for rendering the map of the simulation.
 * It interacts with the MapController to get the state of the map and uses JavaFX to display it.
 */
public class MapRenderer {
    private static final String GRASS_IMAGE_PATH = "/grass.jpeg";
    private static final String MARKED_GRASS_IMAGE_PATH = "/marked_grass.png";
    private static final String[] ANIMAL_IMAGE_PATHS = {
            "/lion.jpeg",
            "/cheetah.jpeg",
            "/rs.jpeg",
            "/elephant.jpeg",
            "/zebra.png"
    };

    private MapController mapController;
    private GridPane gridPane;
    private Text timeText;
    private int squareSize;
    private Map<String, Image> animalImages = new HashMap<>();
    private Time time;
    /**
     * Constructor for the MapRenderer class. Initializes a new MapRenderer with the specified parameters.
     *
     * @param mapController The MapController object that is responsible for updating the state of the map.
     * @param time The Time object that keeps track of the simulation time.
     * @param squareSize The size of each square in the grid.
     */
    public MapRenderer(MapController mapController, Time time, int squareSize) {
        this.mapController = mapController;
        this.time = time;
        this.squareSize = squareSize;
        loadAnimalImages();
    }
    /**
     * This method is used to initialize the map view.
     * It creates the map, sets up the layout, and displays the primary stage.
     *
     * @param primaryStage The primary stage of the JavaFX application.
     * @throws FileNotFoundException If an image file cannot be found.
     */
    public void initializeMapView(Stage primaryStage) throws FileNotFoundException {
        mapController.createMap();
        gridPane = createMapView();
        BorderPane borderPane = new BorderPane();
        borderPane.setCenter(gridPane);

        timeText = new Text();
        timeText.setFont(Font.font(12));
        timeText.setFill(Color.BLACK);
        updateTimeText();

        VBox timeContainer = new VBox(timeText);
        timeContainer.setAlignment(Pos.TOP_LEFT);
        timeContainer.setPadding(new Insets(10));

        HBox buttonContainer = new HBox();
        buttonContainer.setAlignment(Pos.CENTER);
        buttonContainer.setSpacing(10);

        for (int i = 0; i < mapController.chunks.length; i++) {
            Button button = new Button("Chunk " + (i + 1));
            int index = i;
            button.setOnAction(event -> changeChunk(index));
            buttonContainer.getChildren().add(button);
        }

        VBox bottomContainer = new VBox();
        bottomContainer.getChildren().addAll(buttonContainer);
        bottomContainer.setAlignment(Pos.CENTER);

        borderPane.setBottom(bottomContainer);
        borderPane.setTop(timeContainer);

        Scene scene = new Scene(borderPane);
        primaryStage.setScene(scene);
        primaryStage.sizeToScene();
        primaryStage.centerOnScreen();

        primaryStage.show();
        updateMapView();
    }

    private void loadAnimalImages() {
        try {
            for (String path : ANIMAL_IMAGE_PATHS) {
                Image image = new Image(getClass().getResourceAsStream(path));
                String animalName = extractAnimalNameFromPath(path);
                animalImages.put(animalName, image);
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    private String extractAnimalNameFromPath(String path) {
        String[] parts = path.split("/");
        String filename = parts[parts.length - 1];
        String[] nameParts = filename.split("\\.");
        return nameParts[0];
    }
    /**
     * This method is used to create the map view.
     * It creates the map, sets up the layout, and displays the primary stage.
     * @return GridPane.
     */
    private GridPane createMapView() throws FileNotFoundException {
        GridPane gridPane = new GridPane();
        gridPane.setAlignment(Pos.CENTER);

        Chunk currentChunk = mapController.chunks[mapController.currentChunkIndex];
        int startX = currentChunk.getStartX();
        int endX = currentChunk.getEndX();
        int startY = currentChunk.getStartY();
        int endY = currentChunk.getEndY();

        for (int i = startX; i <= endX; i++) {
            for (int j = startY; j <= endY; j++) {
                ImageView imageView = new ImageView();
                String imagePath = GRASS_IMAGE_PATH;
                if (mapController.mapCreator.map[i][j].containsAnimal) {
                    Animal animal = mapController.mapCreator.map[i][j].getAnimalOnCell();
                    int animalIndex = getAnimalIndex(animal.getName());
                    if (animalIndex >= 0) {
                        imagePath = ANIMAL_IMAGE_PATHS[animalIndex];
                    }
                }
                Image image = new Image(getClass().getResourceAsStream(imagePath));
                imageView.setFitWidth(squareSize);
                imageView.setFitHeight(squareSize);
                imageView.setImage(image);

                gridPane.add(imageView, i - startX, j - startY);
            }
        }
        return gridPane;
    }
    /**
     * This method is used to update the map view.
     * It clears the grid pane and re-adds the image views based on the current state of the map.
     *
     * @throws FileNotFoundException If an image file cannot be found.
     */
    public void updateMapView() throws FileNotFoundException {
        gridPane.getChildren().clear();

        Chunk currentChunk = mapController.chunks[mapController.currentChunkIndex];
        int startX = currentChunk.getStartX();
        int endX = currentChunk.getEndX();
        int startY = currentChunk.getStartY();
        int endY = currentChunk.getEndY();

        for (int i = startX; i <= endX; i++) {
            for (int j = startY; j <= endY; j++) {
                String imagePath;
                ImageView imageView = new ImageView();
                if(mapController.mapCreator.map[i][j].marked){
                    imagePath = MARKED_GRASS_IMAGE_PATH;
                }
                else{
                    imagePath = GRASS_IMAGE_PATH;
                }
                if (mapController.mapCreator.map[i][j].containsAnimal) {
                    Animal animal = mapController.mapCreator.map[i][j].getAnimalOnCell();
                    int animalIndex = getAnimalIndex(animal.getName());
                    if (animalIndex >= 0) {
                        imagePath = ANIMAL_IMAGE_PATHS[animalIndex];
                    }
                }
                Image image = new Image(getClass().getResourceAsStream(imagePath));
                imageView.setImage(image);
                imageView.setFitWidth(squareSize);
                imageView.setFitHeight(squareSize);
                gridPane.add(imageView, i - startX, j - startY);
            }
        }
        updateTimeText();
    }
    /**
     * This updates actual time.
     */
    private void updateTimeText() {
        timeText.setText(time.toString());
    }

    private int getAnimalIndex(String animalName) {
        switch (animalName) {
            case "Lion":
                return 0;
            case "Cheetah":
                return 1;
            case "Rhinoceros":
                return 2;
            case "Elephant":
                return 3;
            case "Zebra":
                return 4;
            default:
                return -1;
        }
    }

    /**
     * This method is used to change the current chunk of the map.
     * It loads the specified chunk and updates the map view.
     *
     * @param index The index of the chunk to load.
     */
    private void changeChunk(int index) {
        mapController.loadChunk(index);
        try {
            updateMapView();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
    /**
     * This method is used to initialize the simulation.
     * It sets up the start button and displays the primary stage.
     *
     * @param primaryStage The primary stage of the JavaFX application.
     */
    public void initializeSimulation(Stage primaryStage) {
        Button startButton = new Button("Start Simulation");
        startButton.setOnAction(event -> startSimulation(primaryStage));

        VBox root = new VBox(startButton);
        root.setAlignment(Pos.CENTER);

        Scene scene = new Scene(root, 400, 400);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
    /**
     * This method is used to start the simulation.
     * It initializes the map view when the start button is clicked.
     *
     * @param primaryStage The primary stage of the JavaFX application.
     */
    private void startSimulation(Stage primaryStage) {
        try {
            initializeMapView(primaryStage);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
